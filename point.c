#include <math.h>
#include "point.h"


double calcDistance(struct Point a, struct Point b){
	
	
	double x = pow(a.x - b.x,2);
	double y = pow(a.y - b.y,2);
	double z = pow(a.z - b.z,2);
	
	return sqrt(x+y+z);
}

Point puntomedio(struct Point P1,struct Point P2){
	double coordx, coordy, coordz;
	struct Point p;

	coordx=((P1.x+P2.x)/2);
	coordy=((P1.y+P2.y)/2);
	coordz=((P1.z+P2.z)/2);
	
	p.x = coordx;
	p.y=coordy;
	p.z=coordz;
 
	return p;
	
}
