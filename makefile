IDIR = ./includes

_DEPS = point.h
DEPS = $(pathsubst %,$(IDIR)/%,$(_DEPS))

program : point.o main.o
	gcc  main.o point.o -o program -I$(IDIR) -lm
	
point.o : point.c $(DEPS) 
	gcc -c point.c -I$(IDIR) -lm
main.o : main.c
	gcc -c main.c -I$(IDIR) -lm

clean :
	rm program main.o point.o

